package gameplay;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import graphics.*;

public class RestartListener implements ActionListener {

	private JFrame gameWindow;

	public RestartListener(JFrame gameWindow) {
		this.gameWindow = gameWindow;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Window.startGame();
		gameWindow.dispose();

	}

}
