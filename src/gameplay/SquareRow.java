package gameplay;

//klass mis realiseerib ruutude rea
public class SquareRow {
	int nr;
	int nrOfSquares;
	Square[] row;
	int squareLength;
	int xCoord;
	int yCoord;

	SquareRow(int nr, int howManySquares, int squareLength, int x, int y) {
		this.nr = nr;
		this.nrOfSquares = howManySquares;
		this.squareLength = squareLength;
		this.xCoord = x;
		this.yCoord = y;
		this.makeARow();
	}

	// ruudu rea masiivi loomine
	private void makeARow() {
		this.row = new Square[this.nrOfSquares];
		for (int i = 0; i < this.nrOfSquares; i++) {
			this.row[i] = new Square(this.xCoord + (i * this.squareLength), this.yCoord, this.squareLength);
		}
	}

	public Square getASquare(int nr) {
		return row[nr];
	}

}
