package gameplay;

import java.util.concurrent.ThreadLocalRandom;

//klass mis k�sitleb k�ike m�ngu v�ljal toimuvat
public class Field {

	private SquareRow[] rows; // masiiv ruutude jaoks
	private int nrOfRows; // info m�ngu suuruste ja asukohtade kohta
	private int rowLength;
	private int xCoord;
	private int yCoord;
	private int nrOfMines;
	private int squareLength;
	private int lastXCoord;
	private int lastYCoord;
	private boolean mineFlag; // boolean v��rtus m�ngu l�pu tuvastamiseks

	public Field(int x, int y, int squareLength, int howManyRows, int howLongRow, int howManyMines) {
		this.xCoord = x;
		this.yCoord = y;
		this.squareLength = squareLength;
		this.nrOfRows = howManyRows;
		this.rowLength = howLongRow;
		this.nrOfMines = howManyMines;
		this.makeAField();
		this.lastXCoord = x + (howLongRow * squareLength);
		this.lastYCoord = y + (howManyRows * squareLength);
		this.mineFlag = false;

	}

	// funktsioon mis loob masiivi ruutudest
	private void makeAField() {
		this.rows = new SquareRow[this.nrOfRows];
		for (int i = 0; i < this.nrOfRows; i++) {
			this.rows[i] = new SquareRow(i, this.rowLength, this.squareLength, this.xCoord,
					this.yCoord + (i * squareLength));
		}
	}

	// funktsioon mis tagastab ruudu
	public Square getASquare(int rowNr, int squareNr) {
		return this.rows[rowNr].getASquare(squareNr);
	}

	// funktsioon mis tegeleb esimese hiire vajutusega, ehk paigutab v�ljale
	// miinid
	public void firstClick(int squareNr, int rowNr) {
		this.rows[rowNr].getASquare(squareNr).setMine(true);
		this.spreadMines(0, 0, this.rowLength, this.nrOfRows, this.nrOfMines);
		this.rows[rowNr].getASquare(squareNr).setMine(false);
	}

	// funktsioon mis tegeleb vasaku hiire nupu vajutamisega
	public void leftClick(int squareNr, int rowNr) {
		if (this.rows[rowNr].getASquare(squareNr).isSeen() == false) {
			this.rows[rowNr].getASquare(squareNr).leftClick();
			if (this.rows[rowNr].getASquare(squareNr).isMine()) {
				this.mineFlag = true;

				// juhul kui satutakse 0 ruudu peale siis muudab n�hatavaks ka
				// k�ik �mber olevad ruudud
			} else if (this.rows[rowNr].getASquare(squareNr).getIntNumber() == 0) {
				if (this.nrIsInField(squareNr - 1, rowNr - 1)) {
					this.leftClick(squareNr - 1, rowNr - 1);
				}
				if (this.nrIsInField(squareNr, rowNr - 1)) {
					this.leftClick(squareNr, rowNr - 1);
				}
				if (this.nrIsInField(squareNr + 1, rowNr - 1)) {
					this.leftClick(squareNr + 1, rowNr - 1);
				}
				if (this.nrIsInField(squareNr - 1, rowNr)) {
					this.leftClick(squareNr - 1, rowNr);
				}
				if (this.nrIsInField(squareNr + 1, rowNr)) {
					this.leftClick(squareNr + 1, rowNr);
				}
				if (this.nrIsInField(squareNr - 1, rowNr + 1)) {
					this.leftClick(squareNr - 1, rowNr + 1);
				}
				if (this.nrIsInField(squareNr, rowNr + 1)) {
					this.leftClick(squareNr, rowNr + 1);
				}
				if (this.nrIsInField(squareNr + 1, rowNr + 1)) {
					this.leftClick(squareNr + 1, rowNr + 1);
				}
			}
		}

	}

	// parema hiire nupuga tegelev funktsioon
	public void rightClick(int squareNr, int rowNr) {
		this.rows[rowNr].getASquare(squareNr).rightClick();
	}

	// miinide paigutamise algoritm (paigutab suvaliselt)
	public void spreadMines(int startSquareNr, int startRowNr, int howLongX, int howLongY, int howManyMines) {
		int i = howManyMines;
		int tempX;
		int tempY;
		while (i > 0) {
			tempX = ThreadLocalRandom.current().nextInt(startSquareNr, startSquareNr + howLongX);
			tempY = ThreadLocalRandom.current().nextInt(startRowNr, startRowNr + howLongY);
			if (this.rows[tempY].getASquare(tempX).isMine() == false) {
				this.makeAMine(tempX, tempY);
				i = i - 1;
			}
		}
	}

	// funktsioon mis tegeleb �hele ruudule miini andmisega ja tema �mbritsevate
	// ruutude numbrite suurendamisega
	public void makeAMine(int squareNr, int rowNr) {
		if (this.rows[rowNr].getASquare(squareNr).isMine() == false) {
			this.rows[rowNr].getASquare(squareNr).setMine(true);
			if (squareNr > 0) {
				this.rows[rowNr].getASquare(squareNr - 1).increaseNumber();
				if (rowNr > 0) {
					this.rows[rowNr - 1].getASquare(squareNr - 1).increaseNumber();
				}
				if (rowNr < this.nrOfRows - 1) {
					this.rows[rowNr + 1].getASquare(squareNr - 1).increaseNumber();
				}
			}
			if (squareNr < this.rowLength - 1) {
				this.rows[rowNr].getASquare(squareNr + 1).increaseNumber();
				if (rowNr > 0) {
					this.rows[rowNr - 1].getASquare(squareNr + 1).increaseNumber();
				}
				if (rowNr < this.nrOfRows - 1) {
					this.rows[rowNr + 1].getASquare(squareNr + 1).increaseNumber();
				}
			}
			if (rowNr > 0) {
				this.rows[rowNr - 1].getASquare(squareNr).increaseNumber();
			}
			if (rowNr < this.nrOfRows - 1) {
				this.rows[rowNr + 1].getASquare(squareNr).increaseNumber();
			}
		}

	}

	// kontrollib kas vastava j�rjekorra numbritega ruut on olemas
	public boolean nrIsInField(int squareNr, int rowNr) {
		if (squareNr >= 0 && squareNr < this.rowLength) {
			if (rowNr >= 0 && rowNr < this.nrOfRows) {
				return true;
			}
		}
		return false;
	}

	// kontrolliv kas vastav x ja y kordinaat asuvad m�ngu v�ljal
	public boolean isinField(int x, int y) {
		if (this.xCoord < x && x < this.lastXCoord) {
			if (this.yCoord < y && y < this.lastYCoord) {
				return true;
			}
		}
		return false;
	}

	// funktsioon mis tagastab ruudu mille alla j��b vastav x ja y kordinaat
	public int[] askingForSquare(int x, int y) {
		int[] result = new int[2];
		if (this.isinField(x, y)) {
			result[0] = (x - this.xCoord) / squareLength;
			result[1] = (y - this.yCoord) / squareLength;
		} else {
			result[0] = 0;
			result[1] = 0;
		}

		return result;
	}

	// funktsioon mis loeb lippe
	public int countFlags() {
		int number = 0;
		for (int iy = 0; iy < this.nrOfRows; iy++) {
			for (int ix = 0; ix < this.rowLength; ix++) {
				if (this.rows[iy].getASquare(ix).isFlag()) {
					number++;
				}
			}
		}
		return number;
	}

	// funktsioon mis kontrollib kas m�ng on v�idetud
	public boolean haveYouWon() {
		boolean state = true;
		for (int iy = 0; iy < this.nrOfRows; iy++) {
			for (int ix = 0; ix < this.rowLength; ix++) {
				if (this.rows[iy].getASquare(ix).isMine() == false && this.rows[iy].getASquare(ix).isSeen() == false) {
					state = false;
				}
			}
		}

		return state;
	}

	// funktsioon mis muudab k�ik ruudud n�htavaks
	public void makeAllVisible() {
		for (int iy = 0; iy < this.nrOfRows; iy++) {
			for (int ix = 0; ix < this.rowLength; ix++) {
				this.rows[iy].getASquare(ix).setSeen(true);

			}
		}
	}

	// getterid ja setterid
	public int getNrOfRows() {
		return nrOfRows;
	}

	public int getRowLength() {
		return rowLength;
	}

	public int getNrOfMines() {
		return nrOfMines;
	}

	public boolean isMineFlag() {
		return mineFlag;
	}

	public int getSquareLength() {
		return squareLength;
	}

	public int getxCoord() {
		return xCoord;
	}

	public int getyCoord() {
		return yCoord;
	}

}
