package graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import gameplay.*;

//klass mis k�sitleb m�ngu men��s raskusvariantide valimist
public class SelectionListener implements ActionListener {

	// int v��rtused mida on vaja vastava raskusega m�ngu alustamiseks
	private int gameFieldX;
	private int gameFieldY;
	private int squareLength;
	private int nrOfColumns;
	private int nrOfRows;
	private int nrOfMines;
	private JFrame menuFrame; // JFrame millel asub men��, v�imaldab p�est nupu
								// vajutamist akna sulgeda

	// loomis funktsioon
	public SelectionListener(int fieldX, int fieldY, int squareLength, int nrOfColumns, int nrOfRows, int nrOfMines) {
		this.gameFieldX = fieldX;
		this.gameFieldY = fieldY;
		this.squareLength = squareLength;
		this.nrOfColumns = nrOfColumns;
		this.nrOfRows = nrOfRows;
		this.nrOfMines = nrOfMines;
	}

	@Override
	// nupule vajutamis funktsioon
	public void actionPerformed(ActionEvent arg0) {
		JFrame window = new JFrame("Minesweeper!"); // loob akna
		Painter content = new Painter(); // loob aknale sisu
		Field gameField = new Field(this.gameFieldX, this.gameFieldY, this.squareLength, this.nrOfColumns,
				this.nrOfRows, this.nrOfMines); // loob m�nguv�lja kajastava
												// objekti
		ScoreBoard gamesScoreBoard = new ScoreBoard(0, 0, gameField.getRowLength() * gameField.getSquareLength(),
				gameField.getyCoord()); // loob m�ngu p�ist kajastava objekti

		// loob aega lugeva objekti ja omistab talle vajalikud objektid
		Clock gamesClock = new Clock();
		gamesClock.setMyScoreBoard(gamesScoreBoard);
		gamesClock.setMyPainter(content);

		// loob hiirevajutusi registreeriva objekti ja omistab talle vajalikud
		// objektid
		Mouse mouseListener = new Mouse();
		mouseListener.setGameField(gameField, gamesScoreBoard);
		mouseListener.setMyPainter(content);
		mouseListener.setMyClock(gamesClock);

		// omistab akna sisule vajalike objekte
		content.setGameField(gameField);
		content.setMyScoreBoard(gamesScoreBoard);
		content.addMouseListener(mouseListener);

		// loob kella mis annab teada kui on m��dunud sekund
		Timer MyTimer = new Timer(1000, gamesClock);
		MyTimer.start();

		// seadistab akna
		window.setContentPane(content);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocation(10, 10);
		window.setSize(gamesScoreBoard.getLength() + 8,
				gamesScoreBoard.getHeight() + (gameField.getNrOfRows() * gameField.getSquareLength()) + 31);
		window.setResizable(false);
		window.setVisible(true);
		mouseListener.setGameWindow(window);

		// sulgeb akna kus see nupp oli paigutatud
		this.menuFrame.dispose();

	}

	// setter

	public void setMenuFrame(JFrame menuFrame) {
		this.menuFrame = menuFrame;
	}

}
