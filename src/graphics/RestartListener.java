package graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import graphics.*;

//klass mis taask�evitab m�ngu
public class RestartListener implements ActionListener {

	private JFrame gameWindow;
	private JFrame ownWindow;

	public RestartListener(JFrame gameWindow, JFrame ownWindow) {
		this.gameWindow = gameWindow;
		this.ownWindow = ownWindow;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// kutsub v�lja main meetodis oleva startGame() funktsiooni ja h�vitab
		// vana m�ngu akna ja ka akna kus nupp ise oli
		Window.startGame();
		gameWindow.dispose();
		ownWindow.dispose();

	}

}
