package gameplay;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import graphics.*;

//klass mis reageerib hiire vajutustele
public class Mouse implements MouseListener {

	private Field gameField;
	private Painter myPainter;
	private Clock myClock;
	private ScoreBoard myScoreBoard;
	private JFrame gameWindow;

	private int[] squareNumbers;

	private boolean firstClick = false; // t�ene kui esimne vajutus on �ra olnud
	private boolean isGameOn = true; // t�ene kui m�ng k�ib

	@Override
	public void mouseClicked(MouseEvent arg0) {

		if (isGameOn) { // boolean v��rtus kontrollib m�ngu olekut
			// funktsioon mis kontrollib kas vajutati v�ljale
			if (this.gameField.isinField(arg0.getX(), arg0.getY())) {
				// k�sib mis ruut asub vajutuse kordinaatdel
				this.squareNumbers = this.gameField.askingForSquare(arg0.getX(), arg0.getY());

				// reageerib vasakule hiirevajutusele
				if (arg0.getButton() == 1) {
					// left mouse button
					// m�ng paigaldab v�ljale miinid alles p�rast esimest
					// vajutust (ei ole v�imalik kaotada esimese valikuga)
					if (this.firstClick == false) {
						this.gameField.firstClick(squareNumbers[0], squareNumbers[1]);
						this.gameField.leftClick(this.squareNumbers[0], this.squareNumbers[1]);
						this.firstClick = true;
						this.myClock.unPause();

					} else {
						this.gameField.leftClick(this.squareNumbers[0], this.squareNumbers[1]);
						// kontrollib ega ei sattutud miinile
						if (this.gameField.isMineFlag()) {
							this.gameField.makeAllVisible();
							this.myClock.pause();
							this.isGameOn = false;
							this.myPainter.setGameOn(false);
							this.endGameActions("You have lost!");
							// k�sib m�ngu v�lja objektil ega ei v�idetud
						} else if (this.gameField.haveYouWon()) {
							this.myClock.pause();
							this.isGameOn = false;
							this.myPainter.setGameOn(false);
							this.endGameActions("You have won!");
						}
					}
					this.myPainter.repaint();
				}
				if (arg0.getButton() == 3 && this.firstClick) {
					// right mouse button
					this.gameField.rightClick(this.squareNumbers[0], this.squareNumbers[1]);
					this.myScoreBoard.setNumberOfMines(this.gameField.getNrOfMines() - this.gameField.countFlags());
					this.myPainter.repaint();
				}
			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	// funktsioon mis k�istleb m�ngu l�ppu tegevust, ehk loob uuesti alustamise
	// jaoks akna ja nupu
	private void endGameActions(String result) {
		JFrame window = new JFrame(result);
		JButton restartButton = new JButton(result + ", restart?");
		RestartListener restartListener = new RestartListener(this.gameWindow, window);
		restartButton.addActionListener(restartListener);
		JPanel content = new JPanel();
		content.setLayout(new GridLayout(1, 1));
		content.add(restartButton);
		window.setContentPane(content);
		window.setSize(250, 250);
		window.setLocation(100, 100);
		window.setVisible(true);

	}

	// getterit ja setterid
	public void setGameField(Field gameField, ScoreBoard myScoreBoard) {
		this.gameField = gameField;
		this.myScoreBoard = myScoreBoard;
		this.myScoreBoard.setNumberOfMines(this.gameField.getNrOfMines() - this.gameField.countFlags());
	}

	public void setMyPainter(Painter myPainter) {
		this.myPainter = myPainter;
	}

	public void setMyClock(Clock myClock) {
		this.myClock = myClock;
	}

	public void setGameWindow(JFrame gameWindow) {
		this.gameWindow = gameWindow;
	}

}
