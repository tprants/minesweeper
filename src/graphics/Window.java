package graphics;

import javax.swing.*;
import java.awt.*;

import gameplay.*;

public class Window {

	public static void main(String[] args) {
		// k�ivitab m�ngu
		startGame();

	}

	// iseseisev funktsioon mis on m�eldud m�ngu algus punktiks, loob
	// raskuastmete valimise nupud
	public static void startGame() {
		// loob nupud
		JButton easyButton = new JButton("easy");
		JButton mediumButton = new JButton("medium");
		JButton hardButton = new JButton("hard");

		// loob nupu vajutamisele reageerivad objektid
		SelectionListener easyListener = new SelectionListener(0, 100, 50, 9, 9, 10);
		SelectionListener mediumListener = new SelectionListener(0, 100, 50, 16, 16, 40);
		SelectionListener hardListener = new SelectionListener(0, 100, 50, 16, 30, 99);

		// seob nupud kuulajatega
		easyButton.addActionListener(easyListener);
		mediumButton.addActionListener(mediumListener);
		hardButton.addActionListener(hardListener);

		// loob aknale sisu ja lisab sinna nupud
		JPanel content = new JPanel();
		content.setLayout(new GridLayout(3, 1));
		content.add(easyButton);
		content.add(mediumButton);
		content.add(hardButton);

		// loob akna ja seadistab selle
		JFrame window = new JFrame("Minesweeper!");
		easyListener.setMenuFrame(window);
		mediumListener.setMenuFrame(window);
		hardListener.setMenuFrame(window);
		window.setContentPane(content);
		window.setSize(250, 250);
		window.setLocation(100, 100);
		window.setVisible(true);
	}

}
