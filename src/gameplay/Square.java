package gameplay;

//klass mis realiseerib m�nguv�ljal �he ruudu
public class Square {
	private int xCoord;
	private int yCoord;
	private int length;
	private boolean isMine;
	private boolean isSeen = false;
	private boolean isFlag = false;
	private int intNumber;
	private String number;

	public Square(int x, int y, int length) {
		this.xCoord = x;
		this.yCoord = y;
		this.length = length;
		this.intNumber = 0;
		this.number = Integer.toString(this.intNumber);
	}

	// funktsioon mis tegeleb vasaku hiire vajutusega
	public void leftClick() {
		if (isSeen == false && isFlag == false) {
			this.isSeen = true;
		}
	}

	// funktsioon mis tegeleb parema hiire vajutusega
	public void rightClick() {
		if (isSeen == false) {
			if (isFlag == false) {
				this.isFlag = true;
			} else {
				this.isFlag = false;
			}
		}
	}

	// funktsioon mis suurendab ruudu numbrit
	public void increaseNumber() {
		this.intNumber++;
		this.number = Integer.toString(this.intNumber);
	}

	// getterid ja setterid
	public boolean isMine() {
		return isMine;
	}

	public void setMine(boolean isMine) {
		this.isMine = isMine;
	}

	public int getxCoord() {
		return xCoord;
	}

	public int getyCoord() {
		return yCoord;
	}

	public int getLength() {
		return length;
	}

	public void setXcoord(int xcoord) {
		this.xCoord = xcoord;
	}

	public void setyCoord(int yCoord) {
		this.yCoord = yCoord;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public boolean isSeen() {
		return isSeen;
	}

	public void setSeen(boolean isSeen) {
		this.isSeen = isSeen;
	}

	public boolean isFlag() {
		return isFlag;
	}

	public void setFlag(boolean isFlag) {
		this.isFlag = isFlag;
	}

	public int getIntNumber() {
		return intNumber;
	}

}
