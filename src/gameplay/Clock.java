package gameplay;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import graphics.*;

//funktsioon mis tegeleb aja lugemisega
public class Clock implements ActionListener {

	private int time = 0;
	private boolean pause = true; // vőimaldab aja lugemist peatada

	private ScoreBoard myScoreBoard;
	private Painter myPainter;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (!pause) {
			this.time++;
			if (time < 1000) { // loeb aega kuni 999-ni
				this.myScoreBoard.setTime(this.time);
				this.myPainter.repaint();
			}
		}

	}

	// getterid ja setterid

	public int getTime() {
		return time;
	}

	public void setMyScoreBoard(ScoreBoard myScoreBoard) {
		this.myScoreBoard = myScoreBoard;
	}

	public void unPause() {
		this.pause = false;
	}

	public void pause() {
		this.pause = true;
	}

	public void setMyPainter(Painter myPainter) {
		this.myPainter = myPainter;
	}

}
