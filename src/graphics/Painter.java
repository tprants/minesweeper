package graphics;

import java.awt.*;
import javax.swing.JPanel;
import gameplay.*;

//Klass tegeleb m�ngu akna sisu joonistamisega (sisu on jaotatud p�iseks ja m�nguv�ljaks)

public class Painter extends JPanel {

	private Field gameField; // objekt mis k�sitleb m�nguv�lja
	private Square tempSquare; // koodi sees kasutatud muutuja millele
								// salvestatakse ruudu objekte
	private ScoreBoard myScoreBoard; // objekt mis k�sitleb m�ngu akna p�ist
	private int tempX; // koodi sees kasutatud muutuja x kordinaatide jaoks
	private int tempY; // koodi sees kasutatud muutuja y kordinaatide jaoks
	private int tempLength; // koodi sees kasutatud muutuja pikkuste jaoks
	private int tempHeight; // koodi sees kasutatud muutuja k�rguste jaoks
	private boolean isGameOn = true; // binaar v��rtus, mis n�itab kas m�ng k�ib
										// (kasutatakse valesti pandud lippude
										// tuvastamiseks)

	// �ldine paint funktsioon
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);

		this.drawAScoreBoard(g);
		this.paintSquares(g);

	}

	// eraldi paint funktsioon mis k�sitleb m�nguv�ljal asetsevaid m�ngu ruute
	public void paintSquares(Graphics g) {
		// ts�kkel mis �kshaaval joonistab ruute
		for (int i = 0; i < this.gameField.getNrOfRows(); i++) {
			for (int i2 = 0; i2 < this.gameField.getRowLength(); i2++) {
				this.tempSquare = this.gameField.getASquare(i, i2);
				this.tempX = this.tempSquare.getxCoord();
				this.tempY = this.tempSquare.getyCoord();
				this.tempLength = this.tempSquare.getLength();

				g.drawRect(this.tempX, this.tempY, this.tempLength, this.tempLength);
				g.setColor(Color.LIGHT_GRAY);
				g.fillRect(tempX + 1, tempY + 1, tempLength - 1, tempLength - 1);
				g.setColor(Color.BLACK);

				// vajadusel joonistab ruudule lipu
				if (this.tempSquare.isFlag()) {
					if (this.isGameOn) {
						this.drawAFLag(g, tempX, tempY);
						g.setColor(Color.BLACK);
					}
					// m�ngu l�ppedes tuleb eristada valesti pandud lippe
					if (this.isGameOn == false) {
						if (this.tempSquare.isMine()) {
							this.drawAFLag(g, tempX, tempY);
							g.setColor(Color.BLACK);
						} else {
							g.setColor(Color.RED);
							g.setFont(new Font("TimesRoman", Font.PLAIN, this.gameField.getSquareLength()));
							g.drawString("X", tempX + (tempLength / 6), tempY + (tempLength / 5) * 4);
							g.setColor(Color.BLACK);
						}
					}
					// joonistab avatud ruudud teist v�rvi ja vajadusel vastava
					// numbri v�i miini peale
				} else if (this.tempSquare.isSeen()) {
					if (this.tempSquare.isMine()) {
						g.drawString("*", tempX + (tempLength / 4), tempY + (tempLength));
					} else {
						g.setColor(Color.CYAN);
						g.fillRect(tempX + 1, tempY + 1, tempLength - 1, tempLength - 1);
						g.setColor(Color.BLACK);
						if (this.tempSquare.getIntNumber() != 0) {
							g.setFont(new Font("TimesRoman", Font.PLAIN, this.gameField.getSquareLength()));
							g.drawString(this.tempSquare.getNumber(), tempX + (tempLength / 4),
									tempY + (tempLength / 5) * 4);
						}
					}
				}
			}
		}
	}

	// lipu joonistamis funktsioon
	public void drawAFLag(Graphics g, int x, int y) {
		int relativeLength = gameField.getSquareLength();
		g.drawLine(x + (relativeLength / 3), y + (relativeLength / 10) * 2, x + (relativeLength / 3),
				y + (relativeLength / 5) * 4);
		g.drawRect(x + (relativeLength / 3), y + (relativeLength / 10) * 2, relativeLength / 3, relativeLength / 3);
		g.setColor(Color.RED);
		g.fillRect(x + (relativeLength / 3) + 1, y + ((relativeLength / 10) * 2) + 1, (relativeLength / 3),
				(relativeLength / 3) - 1);
		g.setColor(Color.BLACK);
	}

	// m�nguv�lja p�ise joonistamis funktsioon (aeg ja avastamata miinide arv)
	public void drawAScoreBoard(Graphics g) {
		this.tempX = this.myScoreBoard.getX();
		this.tempY = this.myScoreBoard.getY();
		this.tempLength = this.myScoreBoard.getLength();
		this.tempHeight = this.myScoreBoard.getHeight();

		g.drawRect(this.tempX, this.tempY, this.tempLength, this.tempHeight);
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(this.tempX + 1, this.tempY + 1, this.tempLength - 1, this.tempHeight - 1);
		g.setColor(Color.BLACK);
		g.setFont(new Font("TimesRoman", Font.PLAIN, this.tempHeight / 2));
		g.drawString(this.myScoreBoard.getTimeString(), this.tempX + (this.tempHeight / 6),
				this.tempY + (this.tempHeight / 3) * 2);
		g.drawString(this.myScoreBoard.getFlagString(), this.tempX + (this.tempLength - (this.tempHeight / 3) * 7),
				this.tempY + (this.tempHeight / 3) * 2);
	}

	// getterid ja setterid
	public void setGameField(Field gameField) {
		this.gameField = gameField;
	}

	public void setMyScoreBoard(ScoreBoard myScoreBoard) {
		this.myScoreBoard = myScoreBoard;
	}

	public void setGameOn(boolean isGameOn) {
		this.isGameOn = isGameOn;
	}

}
