package gameplay;

//klass mis realiseerib m�ngu p�ise
public class ScoreBoard {

	private int x;
	private int y;
	private int length;
	private int height;
	private int time;
	private String timeString;
	private int numberOfMines;
	private String mineString;

	public ScoreBoard(int xCoordinate, int yCoordinate, int length, int height) {
		this.x = xCoordinate;
		this.y = yCoordinate;
		this.length = length;
		this.height = height;
		this.time = 0;
		this.numberOfMines = 0;
		this.timeString = "Time:000";
		this.mineString = "Mines:000";

	}

	// getterid ja setterid
	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
		// uuendab aja string v��rtust
		if (this.time < 10) {
			this.timeString = "Time:00" + this.time;
		} else if (this.time < 100) {
			this.timeString = "Time:0" + this.time;
		} else {
			this.timeString = "Time:" + this.time;
		}

	}

	public int getNumberOfFlags() {
		return numberOfMines;
	}

	public void setNumberOfMines(int numberOfFlags) {
		this.numberOfMines = numberOfFlags;
		// uuendab miinide string v��rtust
		if (this.numberOfMines >= 0) {
			if (this.numberOfMines < 10) {
				this.mineString = "Mines:00" + this.numberOfMines;
			} else if (this.numberOfMines < 100) {
				this.mineString = "Mines:0" + this.numberOfMines;
			} else if (this.numberOfMines < 1000) {
				this.mineString = "Mines:" + this.numberOfMines;
			} else {
				this.mineString = "Mines:999";
			}
		} else {
			if (this.numberOfMines > -10) {
				this.mineString = "Mines:-00" + (this.numberOfMines * -1);
			} else if (this.numberOfMines > -100) {
				this.mineString = "Mines:-0" + (this.numberOfMines * -1);
			} else if (this.numberOfMines > -11000) {
				this.mineString = "Mines:-" + (this.numberOfMines * -1);
			} else {
				this.mineString = "Mines:-999";
			}
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getLength() {
		return length;
	}

	public int getHeight() {
		return height;
	}

	public String getTimeString() {
		return timeString;
	}

	public String getFlagString() {
		return mineString;
	}

}
